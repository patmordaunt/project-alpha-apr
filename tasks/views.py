from django.shortcuts import render, redirect
from .models import Task
from .forms import TaskForm
from django.contrib.auth.decorators import login_required


# Create your views here.
def show_tasks(request):
    tasks = Task.objects.all()
    context = {
        "show_tasks": tasks,
    }

    return render(request, "projects/details.html", context)


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            project = form.save()
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
        context = {
            "form": form,
        }
    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    task = Task.objects.filter(assignee=request.user)
    context = {
        "show_my_tasks": task,
    }
    return render(request, "projects/list.html", context)
